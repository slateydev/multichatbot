﻿using ServiceConnector;
using System;
using System.Windows.Controls;

namespace ServiceConnector_XMPP
{
    public class ChatServiceDefinition : IChatServiceDefinition
    {
        public string Name
        {
            get
            {
                return "Livecoding.tv (XMPP)";
            }
        }

        public Type ChatServiceType
        {
            get
            {
                return typeof(ChatServiceImplementation);
            }
        }

        public UserControl ConfigControl
        {
            get
            {
                return new ConfigControlImplementation();
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}