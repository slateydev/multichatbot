﻿using ServiceConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ServiceConnector_XMPP
{
    public class ChatServiceImplementation : ChatService
    {
        private const string bosh = "/chat/http-bind";
        private const string ws = "/chat/websocket";

        private string roomName = null;
        private string name = null;
        private string password = null;

        private string roomJid = null;
        private string jid = null;
        private string resource = null;

        private ClientWebSocket socket;
        private int state = 0;

        public override void Initialize(List<ServiceParameter> parameters)
        {
            name = parameters.Where(p => p.Key == "User").Select(p => p.Value).FirstOrDefault();
            password = parameters.Where(p => p.Key == "Password").Select(p => p.Value).FirstOrDefault();
            roomName = parameters.Where(p => p.Key == "RoomName").Select(p => p.Value).FirstOrDefault();

            if(name == null || password == null || roomName == null)
            {
                throw new Exception("Invalid parameters");
            }

            roomJid = roomName + "@chat.livecoding.tv";
            jid = name + "@livecoding.tv";
            resource = "web-" + roomName + "-0ejREgYF";
        }

        public override void Connect()
        {
            if (name == null || password == null || roomName == null)
            {
                throw new Exception("Parameters not set correctly");
            }

            socket = new ClientWebSocket();
            socket.Options.SetRequestHeader("Sec-WebSocket-Protocol", "xmpp");
            socket.ConnectAsync(new Uri("wss://www.livecoding.tv/chat/websocket"), CancellationToken.None).Wait();
            Task.WhenAll(Receive(socket), Send(socket, "<open xmlns='urn:ietf:params:xml:ns:xmpp-framing' to='livecoding.tv' version='1.0' />"));
        }

        private const int receiveChunkSize = 2048;
        private readonly TimeSpan delay = TimeSpan.FromMilliseconds(3000);

        private async Task Send(ClientWebSocket webSocket, string Message)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(Message);
            await webSocket.SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Text, true, CancellationToken.None);
            //OnMessageArrived("send", null, Message, null);
        }

        private async Task Receive(ClientWebSocket webSocket)
        {
            byte[] buffer = new byte[receiveChunkSize];
            while (webSocket.State == WebSocketState.Open)
            {
                var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                }
                else
                {
                    var recvbuffer = Encoding.UTF8.GetString(buffer, 0, result.Count);
                    //OnMessageArrived("receive", null, recvbuffer, null);

                    XDocument doc = XDocument.Parse(recvbuffer);
                    foreach (XElement element in doc.Nodes())
                    {
                        if (element.Name.Equals(XName.Get("features", "http://etherx.jabber.org/streams")))
                        {
                            foreach(XElement featureselement in element.Nodes())
                            {
                                if(state == 0 && featureselement.Name.Equals(XName.Get("mechanisms", "urn:ietf:params:xml:ns:xmpp-sasl")))
                                {
                                    OnMessageArrived("SYSTEM", null, "Received authentication request", null);

                                    var mechanism = featureselement.Descendants(XName.Get("mechanism", "urn:ietf:params:xml:ns:xmpp-sasl")).First().Value;
                                    if (mechanism == "PLAIN")
                                    {
                                        state = 1;
                                        await Send(webSocket, "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='PLAIN'>" + Convert.ToBase64String(Encoding.UTF8.GetBytes(jid + "\0" + name + "\0" + password)) + "</auth>");
                                    }
                                }
                                else if (state == 2 && featureselement.Name.Equals(XName.Get("bind", "urn:ietf:params:xml:ns:xmpp-bind")))
                                {
                                    OnMessageArrived("SYSTEM", null, "Received bind request", null);

                                    state = 3;
                                    await Send(webSocket, "<iq type='set' id='_bind_auth_2' xmlns='jabber:client'><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'><resource>" + resource + "</resource></bind></iq>");
                                }
                                else
                                {
                                    OnMessageArrived("FEATURES:" + state.ToString(), null, featureselement.ToString(), null);
                                }
                            }
                        }
                        else if (state == 1 && element.Name.Equals(XName.Get("success", "urn:ietf:params:xml:ns:xmpp-sasl")))
                        {
                            OnMessageArrived("SYSTEM", null, "Authentication OK", null);

                            state = 2;
                            await Send(webSocket, "<open xmlns='urn:ietf:params:xml:ns:xmpp-framing' to='livecoding.tv' version='1.0' />");
                        }
                        else if (element.Name.Equals(XName.Get("iq", "jabber:client")))
                        {
                            if (state == 4)
                            {
                                OnMessageArrived("SYSTEM", null, "Session established", null);

                                state = 5;
                                await Send(webSocket, "<presence id='pres:1' xmlns='jabber:client'><priority>1</priority><c xmlns='http://jabber.org/protocol/caps' hash='sha-1' node='https://candy-chat.github.io/candy/' ver='kR9jljQwQFoklIvoOmy/GAli0gA='/></presence>");
                                await Send(webSocket, "<iq type='get' from='" + jid + "' to='" + roomJid + "' xmlns='jabber:client' id='2:sendIQ'><query xmlns='http://jabber.org/protocol/disco#info'/></iq>");
                                await Send(webSocket, "<presence to='" + roomJid + "/" + name + "' id='pres:3' xmlns='jabber:client'><x xmlns='http://jabber.org/protocol/muc'/><c xmlns='http://jabber.org/protocol/caps' hash='sha-1' node='https://candy-chat.github.io/candy/' ver='kR9jljQwQFoklIvoOmy/GAli0gA='/></presence>");
                                await Send(webSocket, "<iq type='get' from='" + jid + "' xmlns='jabber:client' id='4:sendIQ'><query xmlns='jabber:iq:privacy'><list name='ignore'/></query></iq>");
                            }

                            foreach (XElement iqElement in element.Nodes())
                            {
                                if (state == 3 && iqElement.Name.Equals(XName.Get("bind", "urn:ietf:params:xml:ns:xmpp-bind")))
                                {
                                    OnMessageArrived("SYSTEM", null, "Bind", null);

                                    state = 4;
                                    await Send(webSocket, "<iq type='set' id='_session_auth_2' xmlns='jabber:client'><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/></iq>");
                                }
                                else
                                {
                                    OnMessageArrived("IQ:" + state.ToString(), null, iqElement.ToString(SaveOptions.DisableFormatting), null);
                                }
                            }
                        }
                        else if(element.Name.Equals(XName.Get("message", "jabber:client")))
                        {
                            if(state == 5)
                            {
                                //var attributes = element.Attributes();
                                //var from = "message";
                                var from = element.Attributes().Where(a => a.Name.Equals(XName.Get("from", ""))).First().Value;
                                if(from.StartsWith(roomJid + "/"))
                                {
                                    from = from.Substring(roomJid.Length + 1);
                                }
                                var delayed = element.Descendants(XName.Get("delay", "urn:xmpp:delay")).FirstOrDefault();
                                var body = element.Descendants(XName.Get("body", "jabber:client")).First().Value;
                                OnMessageArrived(from, delay != null ? delay.ToString() : null, body, null);
                                //OnMessageArrived("MESSAGE", null, element.ToString(SaveOptions.DisableFormatting), null);
                            }
                            else
                            {
                                OnMessageArrived("MESSAGE:" + state.ToString(), null, element.ToString(SaveOptions.DisableFormatting), null);
                            }
                        }
                        else if(element.Name.Equals(XName.Get("presence", "jabber:client")))
                        {
                            if (state == 5)
                            {
                                var from = element.Attributes().Where(a => a.Name.Equals(XName.Get("from"))).First().Value;
                                if (from.StartsWith(roomJid + "/"))
                                {
                                    from = from.Substring(roomJid.Length + 1);
                                }
                                var x = element.Descendants(XName.Get("x", "http://jabber.org/protocol/muc#user")).FirstOrDefault();
                                if (x != null)
                                {
                                    var xitem = x.Descendants(XName.Get("item", "http://jabber.org/protocol/muc#user")).FirstOrDefault();
                                    if(xitem != null)
                                    {
                                        var type = element.Attributes().Where(a => a.Name.Equals(XName.Get("type"))).FirstOrDefault();
                                        var role = xitem.Attributes().Where(a => a.Name.LocalName == "role").Select(a => a.Value).FirstOrDefault();
                                        var affiliation = xitem.Attributes().Where(a => a.Name.LocalName == "affiliation").Select(a => a.Value).FirstOrDefault();
                                        OnPresenceUpdate(from, role, affiliation, type != null ? type.Value : null);
                                    }
                                }
                            }
                            else
                            {
                                OnMessageArrived("PRESENCE:" + state.ToString(), null, element.ToString(SaveOptions.DisableFormatting), null);
                            }
                        }
                        else
                        {
                            OnMessageArrived("UNKNOWN:" + state.ToString(), null, element.ToString(SaveOptions.DisableFormatting), null);
                        }
                    }
                }
            }
        }

        public override async void SendMessage(string Message)
        {
            string newMessage = "<message type='groupchat' id='13' xmlns='jabber:client' from='" + jid + "' to='" + roomJid + "'><body xmlns='jabber:client'>" + new XText(Message).ToString() + "</body><x xmlns='jabber:x:event'><composing/></x></message>";
            await Send(socket, newMessage);
        }

        public override void Disconnect()
        {
        }
    }
}