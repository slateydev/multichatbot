﻿using ServiceConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServiceConnector_XMPP
{
    /// <summary>
    /// Interaction logic for ConfigControl.xaml
    /// </summary>
    public partial class ConfigControlImplementation : UserControl, IConfigControl
    {
        public ConfigControlImplementation()
        {
            InitializeComponent();
        }

        public ServiceParameter[] parameters
        {
            get
            {
                return new ServiceParameter[] {
                    new ServiceParameter { Key = "User", Value = User.Text },
                    new ServiceParameter { Key = "Password", Value = Password.Password },
                    new ServiceParameter { Key = "RoomName", Value = Room.Text }
                };
            }
        }
    }
}
