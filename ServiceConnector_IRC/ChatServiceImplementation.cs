﻿using ServiceConnector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace ServiceConnector_IRC
{
    public class ChatServiceImplementation : ChatService
    {
        private string roomName = null;
        private string name = null;
        private string password = null;

        private const string hostname = "irc.twitch.tv";
        private const int port = 6667;

        TcpClient socket;

        private int state = 0;

        public override void Initialize(List<ServiceParameter> parameters)
        {
            name = parameters.Where(p => p.Key == "User").Select(p => p.Value).FirstOrDefault();
            password = parameters.Where(p => p.Key == "Password").Select(p => p.Value).FirstOrDefault();
            roomName = parameters.Where(p => p.Key == "RoomName").Select(p => p.Value).FirstOrDefault();

            if (name == null || password == null || roomName == null)
            {
                throw new Exception("Invalid parameters");
            }
        }

        public override void Connect()
        {
            if (name == null || password == null || roomName == null)
            {
                throw new Exception("Parameters not set correctly");
            }

            socket = new TcpClient(hostname, port);
            NetworkStream stream = socket.GetStream();
            var reader = new StreamReader(stream);
            var writer = new StreamWriter(stream);
            Task.WhenAll(Receive(reader, writer), Send(writer, "CAP REQ :twitch.tv/membership"));
        }

        public override void SendMessage(string Message)
        {
        }

        public override void Disconnect()
        {
        }

        private async Task Receive(StreamReader reader, StreamWriter writer)
        {
            bool done = false;
            while (!done)
            {
                string line = await reader.ReadLineAsync();
                if (line == null)
                {
                    OnMessageArrived("IRC", null, "IRC Channel closed", null);
                    done = true;
                    continue;
                }

                switch (state)
                {
                    case 0:
                        state = 1;
                        await Send(writer, "PASS " + password);
                        await Send(writer, "NICK " + name);
                        break;
                    case 1:
                        state = 2;
                        await Send(writer, "JOIN #" + roomName);
                        break;
                    case 2:
                        if (line.Substring(0, 4) == "PING")
                        {
                            await Send(writer, "PONG" + line.Substring(4));
                        }
                        else
                        {
                            var lineitems = line.Split(new char[] { ' ' }, 4);
                            if (lineitems[1] == "JOIN")
                            {
                                OnPresenceUpdate(GetUsername(lineitems[0]), null, null, null);
                            }
                            else if (lineitems[1] == "PART")
                            {
                                OnPresenceUpdate(GetUsername(lineitems[0]), null, null, "unavailable");
                            }
                            else if (lineitems[1] == "PRIVMSG")
                            {
                                OnMessageArrived(GetUsername(lineitems[0]), null, lineitems[3].TrimStart(new char[] { ':' }), null);
                            }
                            else
                            {
                                OnMessageArrived("IRC", null, line, null);
                            }
                        }
                        break;
                }
            }
        }

        private string GetUsername(string ircuser)
        {
            return ircuser.Split(new char[] { '!' }, 2)[0].TrimStart(new char[] { ':' });
        }

        private async Task Send(StreamWriter writer, string Message)
        {
            await writer.WriteLineAsync(Message);
            await writer.FlushAsync();
        }
    }
}
