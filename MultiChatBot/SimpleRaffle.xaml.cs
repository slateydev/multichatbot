﻿using ServiceConnector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MultiChatBot
{
    /// <summary>
    /// Interaction logic for SimpleRaffle.xaml
    /// </summary>
    public partial class SimpleRaffle : Window
    {
        MainWindow mOwner;
        private ObservableCollection<RaffleUser> RaffleMembers = new ObservableCollection<RaffleUser>();

        private int WinnerTimer = 0;
        private RaffleUser Winner = null;
        private Timer winnerTimeout = new Timer(1000);

        public SimpleRaffle(MainWindow owner)
        {
            InitializeComponent();

            lstMembers.ItemsSource = RaffleMembers;

            mOwner = owner;

            mOwner.MessageArrived += Hook_MessageArrived;
            winnerTimeout.Elapsed += winnerTimeout_Elapsed;
        }

        private void winnerTimeout_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (WinnerTimer >= App.RaffleTimeout)
            {
                SetTimerValue("TIMES UP!");
                winnerTimeout.Stop();
                return;
            }

            SetTimerValue(WinnerTimer.ToString());
            WinnerTimer++;
        }

        void Hook_MessageArrived(object sender, MessageEventArgs e)
        {
            if (!App.IsAdmin(e.name))
            {
                if (!RaffleMembers.Any(rm => rm.Username.ToLower() == e.name.ToLower()))
                {
                    RaffleMembers.Add(new RaffleUser { Username = e.name, Eligible = true });
                }
            }

            if (Winner != null)
            {
                if (e.name.ToLower() == Winner.Username.ToLower())
                {
                    txtChat.Text += e.body + Environment.NewLine;
                    winnerTimeout.Stop();
                }
            }
        }

        private void DrawRaffle_Click(object sender, RoutedEventArgs e)
        {
            var EligibleMembers = RaffleMembers.Where(rm => rm.Eligible == true).ToList();
            int memberCount = EligibleMembers.Count;

            if (memberCount == 0)
            {
                return;
            }

            Random rng = new Random();
            int winner = rng.Next(0, memberCount);

            Winner = EligibleMembers[winner];

            txtWinner.Text = Winner.Username;
            Winner.Eligible = false;

            WinnerTimer = 0;
            txtTimer.Text = WinnerTimer.ToString();
            txtChat.Text = string.Empty;

            winnerTimeout.Start();
        }

        private void Player_MouseUp(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement sourceItemsContainer = lstMembers.ContainerFromElement((Visual)e.OriginalSource) as FrameworkElement;
            var user = sourceItemsContainer.DataContext as RaffleUser;
            user.Eligible = !user.Eligible;
        }

        void SetTimerValue(string value)
        {
            Dispatcher.BeginInvoke(new Action(() => { txtTimer.Text = value; }), null);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            mOwner.MessageArrived -= Hook_MessageArrived;
        }
    }
}
