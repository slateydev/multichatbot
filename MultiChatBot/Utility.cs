﻿using ServiceConnector;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;

namespace MultiChatBot
{
    public class Utility
    {
        public static List<IChatServiceDefinition> RefreshModules()
        {
            var moduleInfos = new List<IChatServiceDefinition>();

            var catalog = new AggregateCatalog();

            var files = System.IO.Directory.EnumerateFiles(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Utility)).Location), "ServiceConnector_*.dll").ToList();
            foreach (var file in files)
            {
                var ServiceConnectorAssembly = System.Reflection.Assembly.LoadFile(file);

                var ac = new AssemblyCatalog(ServiceConnectorAssembly);
                var parts = ac.Parts.ToArray();
                catalog.Catalogs.Add(ac);
            }

            //Create the CompositionContainer with the parts in the catalog
            var container = new CompositionContainer(catalog);

            //Fill the imports of this object
            var Plugins = container.GetExports<IChatServiceDefinition>();

            foreach (var item in Plugins)
            {
                moduleInfos.Add(item.Value);
            }

            return moduleInfos;
        }
    }
}