﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MultiChatBot.ViewModel
{
    public class PresenceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
          CultureInfo culture)
        {
            string status = string.Empty;
            switch (value as string)
            {
                case null:
                    status = "joined the channel.";
                    break;
                case "unavailable":
                    status = "left the channel.";
                    break;
            }
            return status;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
          CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
