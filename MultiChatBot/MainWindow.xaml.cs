﻿using MultiChatBot.DataModel;
using MultiChatBot.ViewModel;
using ServiceConnector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MultiChatBot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public event MessageEventHandler MessageArrived;

        private List<UserData> userpoints = new List<UserData>();
        private ObservableCollection<UserData> raffleEligibleUser = new ObservableCollection<UserData>();
        private List<ChatService> services = new List<ChatService>();

        public MainWindow()
        {
            InitializeComponent();

            LoadSettings();

            MCDataContext context = new MCDataContext();
            var accounts = context.Accounts.Include("AccountProperties").ToList();

            //Start chat clients
            foreach (var account in accounts)
            {
                var accountservice = App.modules.Where(s => s.Name == account.Service).FirstOrDefault();
                var newservice = (ChatService)Activator.CreateInstance(accountservice.ChatServiceType);
                newservice.Initialize(account.AccountProperties.Select(p => new ServiceParameter { Key = p.Key, Value = p.Value }).ToList());
                newservice.MessageArrived += Value_MessageArrived;
                newservice.PresenceUpdate += Value_PresenceUpdate;
                newservice.Connect();
                services.Add(newservice);
            }
        }

        private void Value_PresenceUpdate(object sender, ServiceConnector.PresenceEventArgs e)
        {
            switch (e.type)
            {
                case null:
                    lvUser.Items.Add(e.name);
                    break;
                case "unavailable":
                    lvUser.Items.RemoveAt(lvUser.Items.IndexOf(e.name));
                    break;
            }
            AddMessage(e);
        }

        private void Value_MessageArrived(object sender, ServiceConnector.MessageEventArgs e)
        {
            OnMessageArrived(e);
        }

        public ObservableCollection<UserData> UserDataList
        {
            get { return raffleEligibleUser; }
        }

        private void LoadSettings()
        {
            MCDataContext context = new MCDataContext();
            var settings = context.Settings.ToList();
        }

        private void AddMessage(object message)
        {
            bool autoscroll = false;
            ScrollViewer scrollViewer = lvChatRoom.GetVisualChild<ScrollViewer>();
            if (scrollViewer != null)
            {
                autoscroll = (lvChatRoom.Items.Count == scrollViewer.VerticalOffset + scrollViewer.ViewportHeight);
            }

            lvChatRoom.Items.Add(message);
            ScrollLastItemIntoView();

            if(message is MessageEventArgs)
            {
                var msg = message as MessageEventArgs;
                //switch (msg.body)
                //{
                //    case "!taco":
                //        SendAllServices("It's taco time");
                //        break;
                //}
            }
        }

        public void OnMessageArrived(MessageEventArgs e)
        {
            AddMessage(e);

            if (MessageArrived != null)
            {
                Application.Current.Dispatcher.Invoke(() => {
                    MessageArrived(this, e);
                });
            }
        }

        private void ScrollLastItemIntoView()
        {
            if (lvChatRoom.Items.Count > 0)
                lvChatRoom.ScrollIntoView(lvChatRoom.Items[lvChatRoom.Items.Count - 1]);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Disconnect all services
            foreach(var service in services)
            {
                service.Disconnect();
            }
        }

        private void OpenRaffle_Click(object sender, RoutedEventArgs e)
        {
            SimpleRaffle raffle = new SimpleRaffle(this);
            raffle.Show();
        }

        private void PointsSystem_Click(object sender, RoutedEventArgs e)
        {
            PointsSystem pointsSystem = new PointsSystem(this);
            pointsSystem.Show();
        }

        private void SendAllServices(string message)
        {
            if (message.Trim().Length > 0)
            {
                foreach (var service in services)
                {
                    service.SendMessage(message);
                }
            }
        }

        private void txtNewMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var tb = sender as TextBox;
                var message = tb.Text;
                SendAllServices(message);
                tb.Text = "";
            }
        }

        private void AddService_Click(object sender, RoutedEventArgs e)
        {
            AddService addService = new AddService();
            var dialogResult = addService.ShowDialog();
            if (dialogResult == true)
            {
                //Write parameters to DB
                MCDataContext context = new MCDataContext();
                var newAccount = context.Accounts.Add(new Account { Service = addService.ServiceName });
                if(addService.Parameters != null)
                {
                    foreach(var parameter in addService.Parameters)
                    {
                        newAccount.AccountProperties.Add(new AccountProperties { Key = parameter.Key, Value = parameter.Value });
                    }
                }
                context.SaveChanges();

                //Start service
                var accountservice = App.modules.Where(s => s.Name == addService.ServiceName).FirstOrDefault();
                var newservice = (ChatService)Activator.CreateInstance(accountservice.ChatServiceType);
                newservice.Initialize(newAccount.AccountProperties.Select(p => new ServiceParameter { Key = p.Key, Value = p.Value }).ToList());
                newservice.MessageArrived += Value_MessageArrived;
                newservice.PresenceUpdate += Value_PresenceUpdate;
                newservice.Connect();
                services.Add(newservice);
            }
        }
    }
}
