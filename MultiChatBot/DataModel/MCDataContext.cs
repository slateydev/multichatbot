﻿using MultiChatBot.DataModel.Migration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MultiChatBot.DataModel
{
    public class MCDataContext : DbContext
    {
        private static bool Initialized = false;
        private const string DATAFILE = "MCB.dat";

        public MCDataContext()
        {
            if (!Initialized)
            {
                Initialized = true;
                Initialize();
            }

            Database.SetInitializer<MCDataContext>(null);
        }

        private void Initialize()
        {
            Migrator migrator = new Migrator();

            if (!System.IO.File.Exists(DATAFILE))
            {
                //Return 'database doesn't exist'
                migrator.CreateDatabase();
            }

            migrator.DoMigrations();
        }

        public DbSet<Setting> Settings { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountProperties> AccountProperties { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
