﻿using System;
using System.Configuration;
using System.Data.SQLite;
using System.IO;
using System.Linq;

namespace MultiChatBot.DataModel.Migration
{
    public class Migrator
    {
        public void CreateDatabase()
        {
            var MigrationNamespace = (typeof(Migration.Migrator)).Namespace;
            RunMigration(MigrationNamespace + ".DBInit.txt");
        }

        public void DoMigrations()
        {
            MCDataContext ctx = new MCDataContext();

            var MigrationNamespace = (typeof(Migration.Migrator)).Namespace;
            var resources = this.GetType().Assembly.GetManifestResourceNames();

            //Get DBVer to check for a migration
            var currentDbVersion = CurrentVersion(ctx);
            while (resources.Contains(MigrationResourceName(currentDbVersion)))
            {
                //Run the migration commands
                RunMigration(MigrationResourceName(currentDbVersion));

                //Increment the DBVer
                IncrememntDbVersion(ctx, currentDbVersion);

                //Get DBVer to check for a migration
                currentDbVersion = CurrentVersion(ctx);
            }
        }

        private void RunMigration(string MigrationResource)
        {
            var resourceStream = this.GetType().Assembly.GetManifestResourceStream(MigrationResource);
            StreamReader sr = new StreamReader(resourceStream);
            var migrationCommand = sr.ReadToEnd();

            using (SQLiteConnection con = new SQLiteConnection(ConfigurationManager.ConnectionStrings["MCDataContext"].ConnectionString))
            {
                con.Open();

                SQLiteCommand cmd = new SQLiteCommand(migrationCommand, con);
                cmd.ExecuteNonQuery();
            }
        }

        private string CurrentVersion(MCDataContext ctx)
        {
            return ctx.Settings.Where(s => s.Key == "DBVer").Select(s => s.Value).FirstOrDefault();
        }

        private void IncrememntDbVersion(MCDataContext ctx, string CurrentVersion)
        {
            ctx.Settings.Where(s => s.Key == "DBVer").First().Value = (Convert.ToInt32(CurrentVersion) + 1).ToString();
            ctx.SaveChanges();
        }

        private string MigrationResourceName(string Version)
        {
            var MigrationNamespace = (typeof(Migration.Migrator)).Namespace;

            return string.Format("{0}.Update_{1}.txt", MigrationNamespace, Version);
        }
    }
}
