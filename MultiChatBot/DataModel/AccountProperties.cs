﻿namespace MultiChatBot.DataModel
{
    public class AccountProperties
    {
        public long Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public long AccountId { get; set; }

        public virtual Account Account { get; set; }
    }
}
