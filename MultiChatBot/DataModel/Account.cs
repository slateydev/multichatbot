﻿using System.Collections.Generic;

namespace MultiChatBot.DataModel
{
    public class Account
    {
        public Account()
        {
            AccountProperties = new List<AccountProperties>();
        }

        public long Id { get; set; }
        public string Service { get; set; }

        public virtual ICollection<AccountProperties> AccountProperties { get; set; }
    }
}
