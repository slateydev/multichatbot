﻿using System;
using System.Linq;
using System.Windows;

namespace MultiChatBot
{
    /// <summary>
    /// Interaction logic for PointsSystem.xaml
    /// </summary>
    public partial class PointsSystem : Window
    {
        MainWindow mOwner;

        public PointsSystem(MainWindow owner)
        {
            InitializeComponent();

            mOwner = owner;
            lvUserData.ItemsSource = mOwner.UserDataList;
        }

        private void GrabWinner_Click(object sender, RoutedEventArgs e)
        {
            var totalpoints = mOwner.UserDataList.Where(up => up.Points > 0).Sum(up => up.Points);
            Random rng = new Random();
            int winner = rng.Next(1, totalpoints + 1);

            int currentTicket = 1;
            foreach (var userdata in mOwner.UserDataList.Where(up => up.Points > 0))
            {
                if (winner >= currentTicket && winner < currentTicket + userdata.Points)
                {
                    WinnerText.Text = "Ticket #" + winner + ": " + userdata.Username;
                    WinnerText.Visibility = System.Windows.Visibility.Visible;
                    break;
                }
                currentTicket += userdata.Points;
            }
        }
    }
}
