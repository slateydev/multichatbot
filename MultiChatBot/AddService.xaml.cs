﻿using ServiceConnector;
using System.Windows;
using System.Windows.Controls;

namespace MultiChatBot
{
    /// <summary>
    /// Interaction logic for AddService.xaml
    /// </summary>
    public partial class AddService : Window
    {
        private string serviceName;
        private ServiceParameter[] parameters;

        public AddService()
        {
            InitializeComponent();

            ChatServices.ItemsSource = App.modules;
        }

        private void ChatServices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ServiceControl.Content = ((IChatServiceDefinition)ChatServices.SelectedItem).ConfigControl;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            serviceName = ((IChatServiceDefinition)ChatServices.SelectedItem).Name;
            parameters = ((IConfigControl)ServiceControl.Content).parameters;
            this.DialogResult = true;
        }

        public string ServiceName
        {
            get
            {
                return serviceName;
            }
        }

        public ServiceParameter[] Parameters
        {
            get
            {
                return parameters;
            }
        } 
    }
}
