﻿using System.ComponentModel;

namespace MultiChatBot
{
    public class RaffleUser : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _eligible;

        public string Username { get; set; }
        public bool Eligible
        {
            get
            {
                return _eligible;
            }
            set
            {
                _eligible = value;
                NotifyPropertyChanged("Eligible");
            }
        }

        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}