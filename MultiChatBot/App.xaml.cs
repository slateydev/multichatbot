﻿using ServiceConnector;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace MultiChatBot
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static List<IChatServiceDefinition> modules;

        public static string AdminUsers = "";
        public static int RaffleTimeout = 90;
        public static bool AutoJoinOnStartup = false;

        public static bool IsAdmin(string name)
        {
            return AdminUsers.ToLower().Split(',').Contains(name.ToLower());
        }

        public App()
        {
            modules = Utility.RefreshModules();
        }
    }
}
