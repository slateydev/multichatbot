﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace ServiceConnector
{
    [InheritedExport]
    public interface IChatServiceDefinition
    {
        string Name { get; }
        Type ChatServiceType { get; }
        UserControl ConfigControl { get; }
    }
}
