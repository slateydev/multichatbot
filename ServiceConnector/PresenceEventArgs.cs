﻿namespace ServiceConnector
{
    public class PresenceEventArgs
    {
        public string jid { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string affiliation { get; set; }
        public string role { get; set; }
    }
}
