﻿namespace ServiceConnector
{
    public class ServiceParameter
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
