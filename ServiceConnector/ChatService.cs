﻿using System.Collections.Generic;
using System.Windows;

namespace ServiceConnector
{
    public delegate void MessageEventHandler(object sender, MessageEventArgs e);
    public delegate void PresenceEventHandler(object sender, PresenceEventArgs e);

    public abstract class ChatService
    {
        public event MessageEventHandler MessageArrived;
        public event PresenceEventHandler PresenceUpdate;

        public abstract void Initialize(List<ServiceParameter> parameters);
        public abstract void Connect();
        public abstract void SendMessage(string Message);
        public abstract void Disconnect();

        public void OnMessageArrived(string Name, string Timestamp, string Body, string Type)
        {
            if(MessageArrived != null)
            {
                Application.Current.Dispatcher.Invoke(() => {
                    MessageArrived(this, new MessageEventArgs { name = Name, timestamp = Timestamp, body = Body, type = Type });
                });
            }
        }

        public void OnPresenceUpdate(string Name, string Role, string Affiliation, string Type)
        {
            if(PresenceUpdate != null)
            {
                Application.Current.Dispatcher.Invoke(() => {
                    PresenceUpdate(this, new PresenceEventArgs { name = Name, role = Role, affiliation = Affiliation, type = Type });
                });
            }
        }
    }
}
