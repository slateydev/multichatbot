﻿namespace ServiceConnector
{
    public class MessageEventArgs
    {
        public string jid { get; set; }
        public string name { get; set; }
        public string body { get; set; }
        public string type { get; set; }
        public string timestamp { get; set; }
    }
}
